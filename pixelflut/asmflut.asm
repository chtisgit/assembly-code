; This program can play pixelflut
; many things are hardcoded though (IP address, port)

; this is fasm (flat assembler) assembly syntax

format ELF executable
entry _start

include 'config.inc'

segment readable executable

_start:
	; socket()
	mov eax, 0x66 ; sys_socketcall
	mov ebx, 1 ; SYS_SOCKET
	mov ecx, socketblub
	int 80h
	mov [sock], eax

	; connect()
	mov eax, 0x66 ; sys_socketcall
	mov ebx, 3 ; SYS_CONNECT
	mov ecx, connectblub
	mov edx, 16 ; length
	int 80h

	; fork()
	mov eax, 0x02 ; sys_fork
	xor ebx, ebx
	int 80h

	test eax, eax
	jz reader_process

	cmp eax, -1
	je error

	mov eax, 0x6A ; sys_newstat
	mov ebx, filename
	mov ecx, statbuf
	int 80h
	mov eax, [statbuf+20]
	mov [filesize], eax

	mov eax, 0x05 ; sys_open
	mov ebx, filename
	mov ecx, 0 ; flags = O_RDONLY
	xor edx, edx ; mode = 0
	int 80h
	mov [fd], eax

	; send all the file
	mov ebx, [sock]
	mov ecx, [fd]
	;xor edx, edx ; offset
	mov edx, zero
	mov esi, [filesize] ; count
@@:
	mov [zero], 0
	mov eax, 0xBB ; sys_sendfile
	int 80h
	jmp @b

exit:
	xor ebx, ebx
_exit:
	mov eax, 1 ; sys_exit
	int 80h
error:
	mov eax, 1
	jmp _exit


reader_process:
; allocate some memory
	mov eax, 0x2D
	xor ebx, ebx
	int 80h
	
	push eax
	mov ebx, eax
	add ebx, READBUF_SIZE
	push ebx
	mov eax, 0x2D
	int 80h
	pop edx

	cmp eax, edx
	jne error

	pop ecx ; allocated read buffer
	mov ebx, [sock]
	mov edx, READBUF_SIZE
@@:
	mov eax, 0x03 ; sys_read
	int 80h
	test eax,eax
	jns @b
	jmp exit

segment readable writable

filename db FILENAME_STR, 0

socketblub:
dd 2
dd 1
dd 0

connectblub:
sock dd 0
dd sockaddr_in
dd 16

sockaddr_in:
dw 0x0002, ((PORT shr 8) and 0xFF) or ((PORT and 0xFF) shl 8)
dd (IPv4d shl 24) or (IPv4c shl 16) or (IPv4b shl 8) or IPv4a
dd 0, 0

zero dd 0

fd dd ?
filesize dd ?

statbuf:
rd 36
