
; edit this file to fit your needs before assembling


FILENAME_STR equ "commands.txt"

READBUF_SIZE = 1024*1024 ; 1 MB

; connect to IPv4a.IPv4b.IPv4c.IPv4d:PORT
IPv4a = 151
IPv4b = 217
IPv4c = 176
IPv4d = 34
PORT = 1234

