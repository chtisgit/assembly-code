# e-mail verify

e-mail is a program to verify e-mail addresses for validity.
It will only check a subset of the specific RFC.

e-mail runs on Linux only as it uses kernel syscalls directly.
It might run under FreeBSD with linux.ko and it should be
possible to port it easily.

call it with an e-mail address as first parameter.

the make-tlds-asm python script reads TLDs from standard input
and prints an assembly file that contains them to standard
output when it reads EOF.
In the assembly file the strings are nicely aranged to save
memory.
