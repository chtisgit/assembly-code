
import sys
		
def q(s):
	return "\""+str(s)+"\""

class TLD:
	def __init__(self, s):
		self.s = s
		self.split = [0]

	def checksplit(self, s):
		if self.s.endswith(s):
			self.split.append(len(self.s)-len(s)-1)
			return True
		return False

	def echo(self,name,i):
		self.split.sort()
		s = self.s
		minus = 0
		for j in range(0,len(self.split)):
			print name+str(i)+":"
			i+=1
			if len(self.split) <= j+1:
				print "db",q(s),",0"
				break
			n=self.split[j+1]-minus
			print "db", q(s[:n+1])
			s = s[n+1:]
			minus+=n+1
		return i
		
		

tld_strings=[]

for line in sys.stdin:
	tld_strings.append(line.strip())

tld_strings.sort(lambda x,y:-cmp(len(x),len(y)))
count = len(tld_strings)

tlds=[]
for line in tld_strings:
	appended = False
	for t in tlds:
		if t.checksplit(line):
			appended = True
			break
	if not appended:
		tlds.append(TLD(line))


print "tld_count dd",count

i = 0
for t in tlds:
	i = t.echo("s_tld",i)

print "tld_list:"
for i in range(0, count):
	print "dd s_tld"+str(i)


