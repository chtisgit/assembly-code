; less for DOS

org 100h

TEXTBUF_SEG	= 0xB800
TMPBUF_SIZE	= 256
COLS		= 80
LINES		= 25
TABSIZE		= 4

; Get length of command line arguments
movzx cx, byte [80h]
mov si, 81h

; Get file name extracted from command line to 81h
zerofilename1:
	lodsb
	cmp al, ' '
	jne .exit
loop zerofilename1
.exit:
mov dx, si
dec dx
zerofilename2:
	lodsb
	cmp al, 32
	jbe .exit
	cmp al, 127
	jae .exit
loop zerofilename2
.exit:
mov byte [si-1], 0

; Open file
mov ax, 3D00h or 10100000b ; Access mode
int 21h
jc filenotfound
mov [_file], ax

call searchnewlines

; Set cursor
mov ah, 2
xor bh, bh
mov dx, ((LINES-1) shl 8) or (COLS-1)
int 10h

; Clear screen
mov ax, 0xB800
mov es, ax
xor di, di
mov cx, 2000
mov ax, 0x0700
rep stosw
mov ax, ds
mov es, ax


mainloop:
	; Show page
	call fillbuffer
		
	; wait for input
	mov ah, 7
	.input:
		int 21h
		cmp al, 27
		je endprogram
		cmp al, 'q'
		je endprogram
		
		.input_continue:
		test al, al
	jnz .input
	.input_exit:
	
	int 21h
	
	; UP Arrow key HIT?
	cmp al, 48h
	jne .downarr
	
	cmp [_page], 0
	je .continue
	
	dec word [_page]
	
	jmp .continue
	
	; DOWN Arrow key HIT?
	.downarr:
	cmp al, 50h		
	jne .pageup
	
	mov dx, [_lines_count]
	sub dx, LINES-1
	cmp [_page], dx
	jge .continue
	
	inc word [_page]
	jmp .continue
	
	; PageUP key HIT?
	.pageup:
	cmp al, 49h
	jne .pagedown

	mov cx, LINES-1
	cmp [_page], cx
	jge @f
	
	mov cx, [_page]
	
	@@:
	sub [_page], cx		
	
	jmp .continue
	
	; PageDOWN key HIT?
	.pagedown:
	cmp al, 51h		
	jne .continue
	
	mov dx, [_lines_count]
	sub dx, 2*(LINES-1)
	cmp [_page], dx
	mov cx, LINES-1
	jle @f
	
	add dx, LINES-1
	sub dx, [_page]
	mov cx, dx
	
	@@:
	add [_page], cx
	
	jmp .continue
	
	
	

.continue:
jmp mainloop
	
endprogram:

; Close file
mov ah, 3Eh
mov bx, [_file]
int 21h

mov ax, 4C00h
int 21h


; Error "handler"
filenotfound:
	mov dx, err_notfound
	mov ah, 9
	int 21h
jmp endprogram



; Search all the newlines and save them in _lines
; after COLS columns a newline is inserted automatically (-> line wrap)
searchnewlines:

cycles equ word [bp-2]
nonl equ word [bp-4]

	push bp
	mov bp, sp
	sub sp, 4
	mov cycles, 0
	mov nonl, 0
	
	; The first "newline" is at start of the file
	mov di, _lines
	xor ax, ax
	mov [di], ax
	mov [di+2], ax
	add di, 4
	
	.search:
	
		; Read some characters of the file into the buffer _tmp
		push di
		mov ah, 3Fh
		mov bx, [_file]
		mov cx, TMPBUF_SIZE
		mov dx, _tmp
		int 21h
		pop di
		mov cx, ax ; number of bytes read (from 21h)
		jc .search_exit
		jcxz .search_exit
		
		mov si, _tmp
		.count:
			inc nonl
			lodsb
			
			; do we have COLS columns yet?
			cmp nonl, COLS
			jge .calc
			
			; Is it a newline character or not?
			cmp al, 10
			jne .count_continue
			
			
			; Calculate the current position
			.calc:
			mov ax, cycles
			mov bx, TMPBUF_SIZE
			mul bx
			xor bx, bx
			mov bx, TMPBUF_SIZE+1
			sub bx, cx
			add ax, bx
			jnc @f
			inc dx
			@@:
			
			; Add line to list of lines
			stosw
			mov ax, dx
			stosw
			inc word [_lines_count]
			mov nonl, 0
			
		.count_continue:
		loop .count
		
		inc cycles
		
	jmp .search
	.search_exit:
	
.exit:
	mov sp, bp
	pop bp
ret



; Read text to buffer (0xB8000) from line [_page]
fillbuffer:	

cur_line	equ word [bp-2]
cur_col		equ word [bp-4]

	push bp
	mov bp, sp
	sub sp, 4
	
	xor ax, ax
	mov cur_line, ax
	mov cur_col, ax

	mov ax, TEXTBUF_SEG
	mov es, ax
	
	; LSEEK to desired file position
	mov bx, [_page]
	shl bx, 2
	add bx, _lines
	mov dx, [bx]
	mov cx, [bx+2]
	mov bx, [_file]
	mov ax, 4200h
	int 21h
	
	; Fill the text buffer (COLS x LINES)
	xor di, di
	.fill:
		mov ah, 3Fh
		mov bx, [_file]
		mov cx, TMPBUF_SIZE
		mov dx, _tmp
		int 21h
		mov cx, ax ; number of bytes read (from 21h)
		jc .exit_fill
		jcxz .exit_fill
	
		mov si, _tmp
		.copy:
		
			push cx
			lodsb
			
			cmp al, 13	; CR
			je .copy_continue ; don't print CR
			

			cmp al, 9	; TAB
			jne @f 
			
			; Extend a TAB to spaces:
			mov ax, cur_col
			and ax, 3
			mov cx, TABSIZE ; this is the magic number
			sub cx, ax
			add cur_col, cx
			mov ax, 0x0700
			rep stosw
			
			jmp .copy_continue

			@@:
			cmp al, 10	; LF
			jne .nomodify
			
			; fill the rest of the current line with spaces if needed
			mov cx, COLS
			mov ax, cur_col
			sub cx, ax
			jcxz .copy_continue
			mov ax, 0x0700
			rep stosw
			mov cur_col, 80
			
			jmp .copy_continue
			
			; everything else gets printed ;)
			.nomodify:
			mov ah, 0x07
			stosw
			inc cur_col
			
			
		.copy_continue:	
			
			; Look if we reached end of line or end of page
			cmp cur_col, COLS
			jb ._copy_continue
			mov cur_col, 0
			inc cur_line
			cmp cur_line, LINES-1
			je .exit_fill
			
		._copy_continue:
	
			pop cx
		loop .copy
		.copy_exit:

		
	jmp .fill
	.exit_fill:
	
	; Print status bar
	mov di, COLS*(LINES-1)*2
	mov si, ll_title	; TITLE
	@@:
		lodsb
		test al, al
		jz @f
		mov ah, 0x07
		stosw
	jmp @b
	@@:
	mov si, 81h			; FILENAME (from command line memory)
	@@:
		lodsb
		test al, al
		jz @f
		mov ah, 0x07
		stosw
	jmp @b
	@@:
	; clear the rest of the line
	mov cx, COLS*LINES*2
	sub cx, di
	jcxz @f
	shr cx, 1
	mov ax, 0x0700
	rep stosw
	@@:
	

	mov ax, ds
	mov es, ax

	mov sp, bp
	pop bp
ret




; Strings
err_notfound	db "File not found.", 13, 10, "$"
ll_title		db "--- THUNDER-LESS :D --",0

; Data
_page	dw 0	; Number of current line
_lines_count	dw 1 ; saves amount of newlines in file

_file	dw ?	; File handle
_tmp	dw TMPBUF_SIZE dup(?)	; Temporary read buffer

_lines:			; saves position of newlines in file
